import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserModel } from '../_models/user.model';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthHTTPService {
  private baseUrl = `${environment.apiUrl}/User`;
  private TOKEN_DATA_KEY = 'user_access_token';
  private USER_DATA_KEY = 'user_access_data';

  constructor(private http: HttpClient) {}

  async login(Email: string, Password: string): Promise<any> {
    return await this.saveUserData({
      id: '83af3d954408',
      email: 'a@a.com',
      companyName: 'Appenza',
      fullName: 'Abdelhameed',
      userType: 'parent',
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9',
    });
  }

  private saveUserData(response) {
    let userData = JSON.stringify(response);
    localStorage.setItem(this.USER_DATA_KEY, userData);
    localStorage.setItem(this.TOKEN_DATA_KEY, response.token);
  }

  createUser(user: UserModel): Promise<any> {
    return this.http.post(`${this.baseUrl}/RegisterVendor`, user).toPromise();
  }

  checkEmail(email: string): Promise<any> {
    // return this.http
    //   .post(`${this.baseUrl}/forgot-password`, {
    //     email,
    //   })
    //   .toPromise();
    return Promise.resolve(true);
  }
  verifyCode(verificationCode: string): Promise<any> {
    // return this.http.post(`${this.baseUrl}/verify-code`,{
    //   verificationCode,
    // }).toPromise();
    return Promise.resolve(true);
  }
  updatePassword(email: string, password: string): Promise<any> {
    // return this.http.post(`${this.baseUrl}/update-password`,
    // { email,
    //   password,
    // }).toPromise();
    return Promise.resolve(true);
  }
}
