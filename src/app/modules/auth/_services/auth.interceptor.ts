import { AuthService } from "./auth.service";
import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from "@angular/common/http";
import { Observable, throwError, from } from "rxjs";
import { map, catchError, switchMap } from "rxjs/operators";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return from(this.authService.getAccessTokenFromStorage()).pipe(
      switchMap((token) => {
        if (token) {
          request = request.clone({
            headers: request.headers.set("Authorization", "Bearer " + token),
          });
        }

        // if (!request.headers.has('Content-Type')) {
        //   request = request.clone({
        //     headers: request.headers.set('Content-Type', 'application/json'),
        //   });
        // }

        return next.handle(request).pipe(
          map((event: HttpEvent<any>) => {
            return event;
          }),
          catchError((error: HttpErrorResponse) => {
            if (error.status == 401) {
              // this.authService.logout();
            }

            return throwError(error);
          })
        );
      })
    );
  }
}
