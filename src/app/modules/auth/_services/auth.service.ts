import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private TOKEN_DATA_KEY = 'user_access_token';
  private USER_DATA_KEY = 'user_access_data';

  constructor(private router: Router) {}

  logout() {
    localStorage.clear();
    this.router.navigateByUrl('/auth/login');
  }

  isUserAuthenticated(): boolean {
    return !!localStorage.getItem(this.TOKEN_DATA_KEY);
  }

  getAccessTokenFromStorage(): string {
    return localStorage.getItem(this.TOKEN_DATA_KEY);
  }

  getUserDataFromStorage() {
    return JSON.parse(localStorage.getItem(this.USER_DATA_KEY));
  }
}
