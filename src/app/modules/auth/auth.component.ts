import { Component, OnInit } from "@angular/core";
import { NgbCarouselConfig } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-auth",
  templateUrl: "./auth.component.html",
  styleUrls: ["./auth.component.scss"],
  providers: [NgbCarouselConfig],
})
export class AuthComponent implements OnInit {
  showNavigationArrows = true;
  showNavigationIndicators = false;
  images: Array<string>;

  today: Date = new Date();

  constructor() {
    this.images = [
      "./assets/media/slider/slider1.jpg",
      "./assets/media/slider/slider2.png",
      "./assets/media/slider/slider3.jpg",
    ];
  }

  ngOnInit(): void {}
}
