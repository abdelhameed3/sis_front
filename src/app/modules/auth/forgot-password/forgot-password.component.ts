import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, Subscription } from "rxjs";
import { AuthService } from "../_services/auth.service";
import { first } from "rxjs/operators";
import { AuthHTTPService } from "../_services/auth-http.service";
import { CommonModule } from "@angular/common";
import { ConfirmPasswordValidator } from "..";

enum ErrorStates {
  NotSubmitted,
  HasError,
  NoError,
}

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.scss"],
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  errorState: ErrorStates = ErrorStates.NotSubmitted;
  errorStates = ErrorStates;
  isLoading$: Observable<boolean>;
  email: string;
  code: string;
  password;
  activeSection = "EmailSection";
  erroMessage: string;

  // private fields
  private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private authHttpService: AuthHTTPService
  ) {
    // this.isLoading$ = this.authService.isLoading$;
  }

  ngOnInit(): void {
    this.initForm();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.forgotPasswordForm.controls;
  }

  initForm() {
    this.forgotPasswordForm = this.fb.group(
      {
        email: [
          "admin@demo.com",
          Validators.compose([
            Validators.required,
            Validators.email,
            Validators.minLength(3),
            Validators.maxLength(320), // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
          ]),
        ],
        password: [
          "",
          Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ]),
        ],
        cPassword: [
          "",
          Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ]),
        ],
        code: ["", Validators.compose([Validators.required])],
      },
      {
        validator: ConfirmPasswordValidator.MatchPassword,
      }
    );
  }

  async checkEmail() {
    console.log(this.forgotPasswordForm.value.email);
    this.email = this.forgotPasswordForm.value.email;

    try {
      if (this.email == null || this.email == "") {
        this.erroMessage = null;
        console.log("Invalid EMail");
        this.erroMessage = "Email is required.";
        return;
      } else {
        console.log("valid email");
        await this.authHttpService.checkEmail(this.email);
        this.erroMessage = "";
        this.activeSection = "CodeSection";
      }
    } catch {
      this.erroMessage = null;
      this.erroMessage = "Cannot find an account with this email";
    }
  }
  async verifyCode() {
    console.log(this.forgotPasswordForm.value.code);
    this.code = this.forgotPasswordForm.value.code;
    try {
      if (this.code == null || this.code == "") {
        this.erroMessage = null;
        this.erroMessage = "Verification Code is required.";
        return;
      } else {
        await this.authHttpService.verifyCode(this.code);
        this.erroMessage = "";
        this.activeSection = "PasswordSection";
      }
    } catch {
      this.erroMessage = null;
      this.erroMessage = "The code you provided is not correct";
    }
  }
  async updatePassword() {
    this.password = this.forgotPasswordForm.value.password;
    console.log(this.email);
    console.log(this.password);
    try {
      if (
        (this.email == null || this.email == "") &&
        (this.password == null || this.password == "")
      ) {
        this.erroMessage = null;
        this.erroMessage = "Email and Password are required.";
        return;
      } else {
        await this.authHttpService.updatePassword(this.email, this.password);
      }
    } catch {
      console.log("Error");
    }
  }
}
