import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '..';
import { AuthHTTPService } from '../_services/auth-http.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: any;
  hasError: boolean;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private authHttpService: AuthHTTPService,
    private router: Router
  ) {
    if (this.authService.isUserAuthenticated()) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.loginForm = this.fb.group({
      Email: [
        '',
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.minLength(3),
          Validators.maxLength(320),
        ]),
      ],
      Password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
    });
  }

  async loginUser() {
    try {
      let user = this.loginForm.value;
      await this.authHttpService.login(user.Email, user.Password);
      this.router.navigate(['/dashboard']);
    } catch {
      this.hasError = true;
    }
  }
}
