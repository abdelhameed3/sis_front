import { Component, OnInit } from '@angular/core';
import { multi } from './data';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  multi: any[];
  view: any[] = [,];

  // options
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = true;
  showLegend: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Government';
  showYAxisLabel: boolean = true;
  yAxisLabel: string = 'Student';
  legendTitle: string = 'Gender';

  colorScheme = {
    domain: ['#3f51b5', '#2196f3', '#AAAAAA'],
  };

  constructor() {
    Object.assign(this, { multi });
  }

  ngOnInit(): void {}
  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }
}
