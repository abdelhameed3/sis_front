export var multi = [
  {
    name: 'Cairo',
    series: [
      {
        name: 'Male',
        value: 100000,
      },
      {
        name: 'Female',
        value: 50000,
      },
    ],
  },

  {
    name: 'Alexandria',
    series: [
      {
        name: 'Male',
        value: 90000,
      },
      {
        name: 'Female',
        value: 30000,
      },
    ],
  },

  {
    name: 'Aswan',
    series: [
      {
        name: 'Male',
        value: 50000,
      },
      {
        name: 'Female',
        value: 20000,
      },
    ],
  },
  {
    name: 'Asyut',
    series: [
      {
        name: 'Male',
        value: 50000,
      },
      {
        name: 'Female',
        value: 10000,
      },
    ],
  },

  {
    name: 'Beheira',
    series: [
      {
        name: 'Male',
        value: 20000,
      },
      {
        name: 'Female',
        value: 40000,
      },
    ],
  },

  {
    name: '	Beni Suef',
    series: [
      {
        name: 'Male',
        value: 40000,
      },
      {
        name: 'Female',
        value: 60000,
      },
    ],
  },
];
